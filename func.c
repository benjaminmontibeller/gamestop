#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "head.h"


void izbornik(GAME* start) {
	int choiceNumber;
	printf("-----G A M E S T O P-----\n");
	printf("1. Insert new game\n2. Search games by name\n3. View all games\n4. Delete archive\n5. Exit\n");
	printf("Choice: ");
	scanf("%d", &choiceNumber);

	switch (choiceNumber) {
	case 1:
		system("cls");
		newGame();
		izbornik(start);
		break;

	case 2:
		system("cls");
		search();
		izbornik(start);
		break;

	case 3:
		system("cls");
		view();
		izbornik(start);
		break;

	case 4:
		system("cls");
		delete();
		izbornik(start);
		break;

	case 5:
		esc();
		break;

	default:
		printf("Choose a number from 1 to 5\n\n");
		izbornik(start);
		break;
	}
	return 0;

}

void newGame(void) {

	GAME* gamepok;
	gamepok = (GAME*)calloc(1, sizeof(GAME));

	if (gamepok == 1)
		perror("Error with allocating memory");

	FILE* fp = NULL;
	fp = fopen("binarna.bin", "ab");

	if (fp == NULL) {
		perror("An error occured while opening the file");
	}
	else {
		printf("---------------------\n");
		printf("~~NEW GAME~~\n");
		printf("---------------------\n");


		printf("Game name: ");
		
		scanf(" %[^\n]s", &gamepok->name);

		printf("Game developer: ");
		
		scanf(" %[^\n]s", &gamepok->developer);

		printf("Game ID: ");

		scanf("%d", &gamepok->ID);

		printf("Amount of copies sold: ");
		scanf("%d", &gamepok->amountSold);
		system("cls"); 
		printf("---------------------\n");
		printf("Game name: %s\nGame developer: %s\nGame ID: %d\nAmount of copies sold: %d\n---------------------\n", gamepok->name, gamepok->developer, gamepok->ID, gamepok->amountSold);
		
	}

	fwrite(gamepok, sizeof(GAME), 1, fp);
	fclose(fp);
	free(gamepok);

	brojac();
	return 0;
}


void view(void) {
	provjera();
	FILE* fp = NULL;
	fp = fopen("binarna.bin", "rb");
	GAME* gamepok = NULL;

	int x = 0;

	if (fp == NULL) {
		perror("An error occured while opening the file");
		
	}
	else {

		fread(&x, sizeof(int), 1, fp);
		if (x == 0) {
			printf("\n---------File is empty---------\n\n\n");
		}
		
		gamepok = (GAME*)calloc(x, sizeof(GAME));

		if (gamepok == 1)
			perror("Error");

		fseek(fp, sizeof(int), SEEK_SET);

		fread(gamepok, sizeof(GAME), x, fp);

		selectionSort(gamepok, x);


		//for petlja za ispis

		for (int i = 0; i < x; i++) {
			printf("\nGAME: %d\n---------------------\nGame name: %s\nGame developer: %s\nGame ID: %d\nAmount of copies sold: %d\n---------------------\n", i + 1, (gamepok + i)->name, (gamepok + i)->developer, (gamepok + i)->ID, (gamepok + i)->amountSold);

			
		}

	}
	fclose(fp);
	free(gamepok);
	return 0;
}

void esc(void) {

	exit(EXIT_SUCCESS);
	return 0;
}

void brojac(void) {

	//odavde
	FILE* br = NULL;
	br = fopen("binarna.bin", "rb+");
	if (br == NULL)
		perror("An error occured while opening the file");
	else {

		int brojac = 0;
		fread(&brojac, sizeof(int), 1, br);
		//dovde da dosegnem brojac
		if (brojac == 0) {

			brojac = 1;
			fseek(br, 0, SEEK_SET);
			fwrite(&brojac, sizeof(int), 1, br);
		}// prvi put
		else {

			brojac++;
			fseek(br, 0, SEEK_SET);
			fwrite(&brojac, sizeof(int), 1, br);
		} // x-ti put

	}
	
	fclose(br);
	return 0;
}

void kreirajdat(void) {

	FILE* pf = NULL;
	pf = fopen("binarna.bin", "wb");
	if (pf == NULL) {
		perror("Error occured while creating the file.\n");
		return;
	}
	else {
		printf("\n~~~~~~File created~~~~~~\n");
		int x = 0;
		fwrite(&x, sizeof(int), 1, pf);
	}
	
	fclose(pf);
	return 0;
}

void provjeradat(void) {

	FILE* fp = fopen("binarna.bin", "rb");
	if (fp == NULL)
		kreirajdat();
	else {
		printf("~~~ File succesfully opened ~~~\n");
		fclose(fp);
	}
	return 0;
}

void provjera(void) {

	FILE* fp = fopen("binarna.bin", "rb");
	if (fp == NULL) {
		printf("\n------File does not exist------\n");
		printf("\n------Creating  new  file------\n");
		kreirajdat();
	}
	else {
		printf("\n---------Games:\n");
		fclose(fp);
	}
	return 0;
}

void delete(void) {

	int x;
	x = remove("binarna.bin");
	if (x == 0) {
		printf("File succesfully deleted.\n");
	}
	else {
		printf("File can't be deleted.\n");
	}
	exit(EXIT_SUCCESS);
	return 0;
}

void search(void) {

	int temp = 0;

	char name[40]; 

	FILE* fp = NULL;
	
	GAME* gamepok = NULL;


	int x = 0;

	fp = fopen("binarna.bin", "rb");

	if (fp == NULL) {
		perror("Error");
	}
	else {

		fread(&x, sizeof(int), 1, fp);

		gamepok = (GAME*)calloc(x, sizeof(GAME));

		if (gamepok == 1)
			perror("Error");

		fread(gamepok, sizeof(GAME), x, fp);

		printf("\nGame's name: ");
		scanf(" %[^\n]s", name);

		//selectionSort(gamepok, x);

		for (int i = 0; i < x; i++) {

			if (strcmp((gamepok + i)->name, name) == 0) {
				temp++;
				printf("\nGAME: %d\n---------------------\nGame name: %s\nGame developer: %s\nGame ID: %d\nAmount of copies sold: %d\n---------------------\n", i + 1, (gamepok + i)->name, (gamepok + i)->developer, (gamepok + i)->ID, (gamepok + i)->amountSold);

			}
		}

		if (temp == 0)
			printf("\n---------Game does not exist---------\n\n");
	}


	fclose(fp);
	free(gamepok);
	return 0;
}

void zamjena(GAME* veci, GAME* manji){



	GAME temp = *manji;
	*manji = *veci;
	*veci = temp;

	return 0;
}

void selectionSort(GAME* allgames, const int numberofgames) { 

	int min = 0;

	for (int i = 0; i < numberofgames - 1; i++)
	{
		min = i;

		for (int j = i + 1; j < numberofgames; j++)
		{
			if ((allgames + j)->amountSold < (allgames + min)->amountSold) {
				min = j;
			}
			

			/*zamjena((allgames + i), (allgames + min));*/
			
		}
		zamjena((allgames + i), (allgames + min));
	}
	return 0;
}