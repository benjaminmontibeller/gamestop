#ifndef HEADER_H
#define HEADER_H
typedef struct game {
	int ID;
	char name[40];
	char developer[40];
	int amountSold;
}GAME;

void izbornik(GAME*);
void newGame(void);
void esc(void);
void brojac(void);
void provjeradat(void);
void view(void);
void delete(void);
void kreirajdat(void);
void search(void);
void zamjena(GAME*, GAME*);
void selectionSort(GAME*, const int);
void provjera(void);

#endif // HEADER_H
